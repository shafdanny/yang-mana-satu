package com.shafiqdaniel.yangmanasatu.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

/**
 * Created by shafiq on 09/12/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class GameSessionTest {

    private GameSession gameSession;
    @Mock MyCountdownTimer myCountdownTimer;

    @Before
    public void setUp() throws Exception {
        gameSession = new GameSession();
        gameSession.setCountdownTimer(myCountdownTimer);
    }

    @Test
    public void addScore() throws Exception {
        assertEquals(0, gameSession.getScore());

        gameSession.addScore();
        assertEquals(1, gameSession.getScore());
    }

    @Test
    public void getScore() throws Exception {
        assertEquals(0, gameSession.getScore());
    }

    @Test
    public void resetSession() throws Exception {
        gameSession.addScore();
        gameSession.addScore();
        assertEquals(2, gameSession.getScore());

        gameSession.resetSession();
        assertEquals(0, gameSession.getScore());
    }

    @Test
    public void setCountdownTimer() throws Exception {

    }

    @Test
    public void startCountdown() throws Exception {

    }

}