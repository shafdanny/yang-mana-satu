package com.shafiqdaniel.yangmanasatu.injection.component;

import com.shafiqdaniel.yangmanasatu.data.local.PreferencesHelper;
import com.shafiqdaniel.yangmanasatu.injection.module.ApplicationModule;
import com.shafiqdaniel.yangmanasatu.model.AnswersCollections;
import com.shafiqdaniel.yangmanasatu.model.FlagDrawable;
import com.shafiqdaniel.yangmanasatu.model.QuestionGenerator;
import com.shafiqdaniel.yangmanasatu.ui.main.MainActivity;
import com.shafiqdaniel.yangmanasatu.ui.question.QuestionActivity;
import com.shafiqdaniel.yangmanasatu.ui.restartpopup.RestartPopupActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by shafiq on 06/12/2016.
 */

@Singleton
@Component(modules =  { ApplicationModule.class })
public interface ApplicationComponent {

    void inject(MainActivity mainActivity);
    void inject(QuestionActivity questionActivity);
    void inject(QuestionGenerator questionGenerator);
    void inject(RestartPopupActivity restartPopupActivity);

    //@ApplicationContext Context context();
    PreferencesHelper preferencesHelper();
    FlagDrawable flagDrawable();

    void inject(AnswersCollections answersCollections);
}
