package com.shafiqdaniel.yangmanasatu.injection.component;

/**
 * Created by shafiq on 07/12/2016.
 */

import com.shafiqdaniel.yangmanasatu.injection.module.ActivityModule;
import com.shafiqdaniel.yangmanasatu.ui.main.MainActivity;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * This component inject dependencies to all Activities across the application
 */
@Singleton
@Subcomponent(modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(MainActivity mainActivity);

}