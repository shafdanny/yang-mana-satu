package com.shafiqdaniel.yangmanasatu.injection.component;

import com.shafiqdaniel.yangmanasatu.injection.ConfigPersistent;
import com.shafiqdaniel.yangmanasatu.injection.module.ActivityModule;

import dagger.Component;

/**
 * Created by shafiq on 07/12/2016.
 */

@ConfigPersistent
@Component(dependencies = ApplicationComponent.class)
public interface ConfigPersistentComponent {

    ActivityComponent activityComponent(ActivityModule activityModule);

}