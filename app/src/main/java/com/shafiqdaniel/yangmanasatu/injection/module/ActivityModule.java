package com.shafiqdaniel.yangmanasatu.injection.module;

import android.app.Activity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by shafiq on 07/12/2016.
 */
@Module
public class ActivityModule {

    private Activity mActivity;

    public ActivityModule(Activity activity) {
        mActivity = activity;
    }

    @Provides
    Activity provideActivity() {
        return mActivity;
    }
}