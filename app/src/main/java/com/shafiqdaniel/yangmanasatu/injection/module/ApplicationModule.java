package com.shafiqdaniel.yangmanasatu.injection.module;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.shafiqdaniel.yangmanasatu.model.AnswersCollections;
import com.shafiqdaniel.yangmanasatu.model.FlagDrawable;
import com.shafiqdaniel.yangmanasatu.model.GameSession;
import com.shafiqdaniel.yangmanasatu.model.QuestionGenerator;
import com.shafiqdaniel.yangmanasatu.utils.AnswersLoader;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by shafiq on 06/12/2016.
 */

@Module
public class ApplicationModule {
    protected final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences(Application application) {
        return PreferenceManager.getDefaultSharedPreferences(application);
    }

    @Provides
    @Singleton
    GameSession provideGameSession() {
        return new GameSession();
    }

    @Provides
    @Singleton
    QuestionGenerator provideQuestionGenerator() { return new QuestionGenerator();}

    @Provides
    @Singleton
    FlagDrawable provideFlagDrawable(Application application) {
        return new FlagDrawable(application);
    }

    @Provides
    @Singleton
    AnswersLoader provideJsonAnsLoader(Application application) {
        return new AnswersLoader(application.getAssets());
    }

    @Provides
    @Singleton
    AnswersCollections provideAnswersCollections() {
        return new AnswersCollections();
    }
}
