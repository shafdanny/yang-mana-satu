package com.shafiqdaniel.yangmanasatu.utils;

import android.content.res.AssetManager;
import android.graphics.drawable.Drawable;

import com.shafiqdaniel.yangmanasatu.model.Answer;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by shafiq on 09/12/2016.
 *
 * Responsible for converting a JSON file to POJOs.
 *
 */

public class AnswersLoader {

    private AssetManager assetManager;
    List<Answer> answerList;

    private final String GAME_ASSETS_ROOT_FOLDER = "CATEGORIES";

    public AnswersLoader(AssetManager assetManager) {
        this.assetManager = assetManager;
        answerList = new ArrayList<>();
        open();
    }

    public void open() {

        Timber.i("Loading assets ... ");
        String[] directories = new String[0];

        try {
            directories = assetManager.list(GAME_ASSETS_ROOT_FOLDER);
        } catch (IOException e) {
            e.printStackTrace();
        }

        for(String dir: directories) {
            Timber.i("Dir found: " + dir);
            String[] files = new String[0];
            try {
                files = assetManager.list(GAME_ASSETS_ROOT_FOLDER + "/" + dir);
            } catch (IOException e) {
                e.printStackTrace();
            }
            for(String file:files) {
                Timber.i("Files found: " + file);
                Answer ans = new Answer();
                ans.setCategory(dir);
                ans.setFileName(file);

                // load image
                try {
                    // get input stream
                    InputStream ims = assetManager.open(GAME_ASSETS_ROOT_FOLDER + "/" + dir + "/" + file);
                    // load image as Drawable
                    Drawable d = Drawable.createFromStream(ims, null);

                    ans.setDrawable(d);
                    ims.close();
                }
                catch(IOException ex) {
                    return;
                }

                Timber.i("" + ans);
                answerList.add(ans);
            }
        }

    }

    public List<Answer> getAnswerList() {
        return answerList;
    }
}
