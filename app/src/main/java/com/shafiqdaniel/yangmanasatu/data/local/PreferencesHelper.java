package com.shafiqdaniel.yangmanasatu.data.local;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by shafiq on 07/12/2016.
 */

@Singleton
public class PreferencesHelper {
    public static final String PREF_FILE_NAME = "yms_pref";
    public static final String SCORE_KEY = "top_score";

    private final SharedPreferences mPref;

    @Inject
    public PreferencesHelper(Application context) {
        mPref = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
    }

    public void clear() {
        mPref.edit().clear().apply();
    }

    public void updateHighestScore(int value) { mPref.edit().putInt(SCORE_KEY,value).apply(); }

    public int getHighestScore() { return mPref.getInt(SCORE_KEY,0); }
}
