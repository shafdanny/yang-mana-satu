package com.shafiqdaniel.yangmanasatu.ui.question;

import android.content.Intent;
import android.os.Bundle;

import com.shafiqdaniel.yangmanasatu.ui.restartpopup.RestartPopupActivity;
import com.shafiqdaniel.yangmanasatu.data.local.PreferencesHelper;
import com.shafiqdaniel.yangmanasatu.model.CountdownTimerEvent;
import com.shafiqdaniel.yangmanasatu.model.GameSession;
import com.shafiqdaniel.yangmanasatu.model.QuestionGenerator;
import com.shafiqdaniel.yangmanasatu.ui.base.BasePresenter;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by shafiq on 08/12/2016.
 */

@Singleton
public class QuestionPresenter extends BasePresenter<QuestionMvpView> implements CountdownTimerEvent{

    @Inject
    PreferencesHelper mPreferencesHelper;

    @Inject
    GameSession mGameSession;

    @Inject
    QuestionGenerator mQuestionGenerator;

    @Inject public QuestionPresenter() {}

    @Override
    public void attachView(QuestionMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public void incrementScore() {
        mGameSession.addScore();
        getMvpView().updateScore(mGameSession.getScore());
    }

    public void saveHighScore() {
        int currentScore = mGameSession.getScore();
        if(currentScore > mPreferencesHelper.getHighestScore()) {
            mPreferencesHelper.updateHighestScore(currentScore);
        }
    }

    public void generateQuestion() {
        mQuestionGenerator.generateNextQuestion(mGameSession.getCategory());
        getMvpView().setImage(mQuestionGenerator.getFlag());
        getMvpView().fillAnswerOptions(mQuestionGenerator.getAnswerOptions());
        int timeMax = mGameSession.setCountdownTimer(this);
        getMvpView().setProgressBarMax(timeMax);
        mGameSession.startCountdown();
    }

    public boolean checkAnswer(String chosen) {
        return mQuestionGenerator.checkAnswer(chosen);
    }

    public void promptRestart(QuestionActivity context) {
        Intent popupIntent = new Intent(context, RestartPopupActivity.class);
        Bundle b = new Bundle();
        b.putInt("score", mGameSession.getScore());
        b.putString("answer", mQuestionGenerator.getCorrectAnswerString());
        popupIntent.putExtras(b);
        context.startActivity(popupIntent);
    }

    @Override
    public void tick(long l) {
        getMvpView().setProgressBarValue((int) l);
    }

    @Override
    public void finish() {
        promptRestart(getMvpView().getQuestionActivity());
    }
}
