package com.shafiqdaniel.yangmanasatu.ui.main;


import com.shafiqdaniel.yangmanasatu.ui.base.MvpView;

/**
 * Created by shafiq on 07/12/2016.
 */

public interface MainMvpView extends MvpView {

    void showAbout();
    void setScore(int score);

}
