package com.shafiqdaniel.yangmanasatu.ui.restartpopup;

import com.shafiqdaniel.yangmanasatu.ui.base.MvpView;

/**
 * Created by shafiq on 09/12/2016.
 */

public interface RestartPopupMvpView extends MvpView {
    void setAnswerText(String answerText);
    void setScoreText(int score);
}
