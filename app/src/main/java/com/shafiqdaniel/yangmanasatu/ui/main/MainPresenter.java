package com.shafiqdaniel.yangmanasatu.ui.main;

import com.shafiqdaniel.yangmanasatu.data.local.PreferencesHelper;
import com.shafiqdaniel.yangmanasatu.ui.base.BasePresenter;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by shafiq on 07/12/2016.
 */
@Singleton
public class MainPresenter extends BasePresenter<MainMvpView> {

    @Inject PreferencesHelper preferencesHelper;

    @Inject
    public MainPresenter() {};

    @Override
    public void attachView(MainMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public void loadQuestion() {}

    public void resetScore() {
        preferencesHelper.clear();
        getMvpView().setScore(0);
    }

    public int getTopScore() {
        return preferencesHelper.getHighestScore();
    }
}
