package com.shafiqdaniel.yangmanasatu.ui.main;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.games.Games;
import com.shafiqdaniel.yangmanasatu.R;
import com.shafiqdaniel.yangmanasatu.injection.component.ApplicationComponent;
import com.shafiqdaniel.yangmanasatu.ui.about.AboutActivity;
import com.shafiqdaniel.yangmanasatu.ui.base.BaseActivity;
import com.shafiqdaniel.yangmanasatu.ui.question.QuestionActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

public class MainActivity
        extends BaseActivity
        implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, MainMvpView {

    private AlertDialog.Builder builder;
    private DialogInterface.OnClickListener dialogClickListener;
    private GoogleApiClient mGoogleApiClient;
    private String LEADERBOARD_ID;

    @BindView(R.id.score_textview_main) TextView topScoreTextView;

    @Inject MainPresenter mMainPresenter;

    @OnClick(R.id.start_button) public void start() {
        Timber.i("Start button clicked");
        Intent activityChangeIntent = new Intent(MainActivity.this, QuestionActivity.class);
        startActivity(activityChangeIntent);
        getWindow().setWindowAnimations(android.R.style.Animation_Translucent);
    }

    @OnClick(R.id.about_button) void about() {
        showAbout();
        Intent activityChangeIntent = new Intent(MainActivity.this, AboutActivity.class);
        startActivity(activityChangeIntent);
    }

    @OnClick(R.id.reset_score_button) void resetScore() {
        Timber.i("Clicking reset button");

        builder.setMessage(getString(R.string.reset_score_prompt_message))
                .setPositiveButton(getString(R.string.yes), dialogClickListener)
                .setNegativeButton(getString(R.string.no), dialogClickListener)
                .show();

    }

    @OnClick(R.id.leaderboard_btn) void viewLeaderboard() {
        if(mGoogleApiClient!=null && !mGoogleApiClient.isConnected())
            mGoogleApiClient.connect();
        if (mGoogleApiClient.isConnected())
            startActivityForResult(Games.Leaderboards.getLeaderboardIntent(mGoogleApiClient,
                    LEADERBOARD_ID), 99);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        builder = new AlertDialog.Builder(this);

        mMainPresenter.attachView(this);

        LEADERBOARD_ID = getString(R.string.LEADERBOARD_ID);

        // Create the Google API Client with access to Plus, Games, and Drive
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Games.API).addScope(Games.SCOPE_GAMES)
                .addApi(Drive.API).addScope(Drive.SCOPE_APPFOLDER)
                .build();

         dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        mMainPresenter.resetScore();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        setScore(mMainPresenter.getTopScore());
    }


    @Override
    protected void injectComponent(ApplicationComponent applicationComponent) {
        applicationComponent.inject(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMainPresenter.detachView();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d("MainActivity", "onConnected: successfully connected !");
        Games.Leaderboards.submitScore(mGoogleApiClient, getString(R.string.LEADERBOARD_ID), mMainPresenter.getTopScore());
    }

    @Override
    public void onConnectionSuspended(int i) {
        // Attempt to reconnect
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("MainActivity", "onConnectionFailed: " + connectionResult);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void showAbout() {
        Timber.i("Showing about");
    }

    @Override
    public void setScore(int score) {
        topScoreTextView.setText(getString(R.string.high_score_text) + " " + score);
    }
}
