package com.shafiqdaniel.yangmanasatu.ui.question;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.shafiqdaniel.yangmanasatu.R;
import com.shafiqdaniel.yangmanasatu.injection.component.ApplicationComponent;
import com.shafiqdaniel.yangmanasatu.ui.base.BaseActivity;

import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

/**
 * Created by shafiq on 10/04/16.
 */
public class QuestionActivity extends BaseActivity implements QuestionMvpView{

    @BindView(R.id.progressbar) ProgressBar progressBar;
    @BindView(R.id.score_textview) TextView scoreTextView;
    @BindView(R.id.image_view_qa) ImageView imageView;

    @BindViews({R.id.choice1, R.id.choice2, R.id.choice3, R.id.choice4})
    List<Button> ansBtnList;

    @Inject QuestionPresenter mQuestionPresenter;

    Button.OnClickListener answerButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            TextView textView = (TextView) findViewById(v.getId());
            String text = (String) textView.getText();
            if(mQuestionPresenter.checkAnswer(text)) {
                mQuestionPresenter.incrementScore();
                mQuestionPresenter.saveHighScore();
                mQuestionPresenter.generateQuestion();
            } else {
                mQuestionPresenter.promptRestart(getQuestionActivity());
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question_answer);
        ButterKnife.bind(this);
        mQuestionPresenter.attachView(this);

        initAnswerButton();

        updateScore(0);

        getWindow().setWindowAnimations(android.R.style.Animation_Translucent);
    }

    @Override
    protected void injectComponent(ApplicationComponent applicationComponent) {
        applicationComponent.inject(this);
    }

    private void initAnswerButton() {

        for(Button button: ansBtnList) {
            button.setOnClickListener(answerButtonListener);
        }

        mQuestionPresenter.generateQuestion();
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        final LinearLayout layout = (LinearLayout)findViewById(R.id.question_answer_linear_layout);

        ViewTreeObserver vto = layout.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                layout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                int width  = layout.getMeasuredWidth();
                int height = layout.getMeasuredHeight();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void updateScore(int score) {
        scoreTextView.setText(Integer.toString(score));
    }

    @Override
    public void fillAnswerOptions(List<String> answerOptions) {
        Iterator<String> ansStringIterator = answerOptions.iterator();
        Iterator<Button> ansBtnIterator = ansBtnList.iterator();

        while(ansStringIterator.hasNext() && ansBtnIterator.hasNext()) {
            String ansString = ansStringIterator.next();
            Button ansBtn = ansBtnIterator.next();

            ansBtn.setText(ansString);
        }
    }

    @Override
    public void setImage(Drawable image) {
        imageView.setImageDrawable(image);
    }

    @Override
    public QuestionActivity getQuestionActivity() {
        return this;
    }

    @Override
    public void setProgressBarMax(int max) {
        progressBar.setMax(max);
    }

    @Override
    public void setProgressBarValue(int value) {
        progressBar.setProgress(value);
    }
}
