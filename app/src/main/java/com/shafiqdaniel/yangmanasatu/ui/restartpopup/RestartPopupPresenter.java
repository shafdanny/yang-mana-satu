package com.shafiqdaniel.yangmanasatu.ui.restartpopup;

import com.shafiqdaniel.yangmanasatu.model.GameSession;
import com.shafiqdaniel.yangmanasatu.model.QuestionGenerator;
import com.shafiqdaniel.yangmanasatu.ui.base.BasePresenter;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by shafiq on 09/12/2016.
 */

@Singleton
public class RestartPopupPresenter extends BasePresenter<RestartPopupMvpView> {

    @Inject
    QuestionGenerator mQuestionGenerator;
    @Inject
    GameSession mGameSession;

    @Inject RestartPopupPresenter() {}

    @Override
    public void attachView(RestartPopupMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }


    public void getCorrectAnswer() {
        String correctAnswer = mQuestionGenerator.getCorrectAnswerString();
        getMvpView().setAnswerText(correctAnswer);
    }


    public void getScore() {
        getMvpView().setScoreText(mGameSession.getScore());
    }

    public void resetSession() {
        mGameSession.resetSession();
    }
}
