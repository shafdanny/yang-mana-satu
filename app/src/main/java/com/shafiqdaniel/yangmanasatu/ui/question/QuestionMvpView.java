package com.shafiqdaniel.yangmanasatu.ui.question;

import android.graphics.drawable.Drawable;

import com.shafiqdaniel.yangmanasatu.ui.base.MvpView;

import java.util.List;

/**
 * Created by shafiq on 08/12/2016.
 */

public interface QuestionMvpView extends MvpView {

    void updateScore(int score);
    void fillAnswerOptions(List<String> answerOptions);
    void setImage(Drawable image);
    QuestionActivity getQuestionActivity();
    void setProgressBarMax(int max);
    void setProgressBarValue(int value);
}
