package com.shafiqdaniel.yangmanasatu.ui.restartpopup;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.widget.TextView;

import com.shafiqdaniel.yangmanasatu.R;
import com.shafiqdaniel.yangmanasatu.injection.component.ApplicationComponent;
import com.shafiqdaniel.yangmanasatu.ui.base.BaseActivity;
import com.shafiqdaniel.yangmanasatu.ui.main.MainActivity;
import com.shafiqdaniel.yangmanasatu.ui.question.QuestionActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by shafiq on 10/04/16.
 */
public class RestartPopupActivity extends BaseActivity implements RestartPopupMvpView{

    @Inject RestartPopupPresenter mRestartPopupPresenter;

    @BindView(R.id.popup_score_text) TextView popupTextView;
    @BindView(R.id.popup_correct_answer_text) TextView correctAnswerTextView;

    @OnClick(R.id.restart_button) void restart() {
        Intent restart = new Intent(RestartPopupActivity.this, QuestionActivity.class);
        restart.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(restart);
        finish();
    }

    @OnClick(R.id.exit_button) void exit() {
        Intent exit = new Intent(RestartPopupActivity.this, MainActivity.class);
        exit.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(exit);
        finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.restart);
        ButterKnife.bind(this);
        mRestartPopupPresenter.attachView(this);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width*0.8), (int)(height*0.5));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            getWindow().setElevation(24);
        }

        mRestartPopupPresenter.getCorrectAnswer();
        mRestartPopupPresenter.getScore();
        mRestartPopupPresenter.resetSession();
    }

    @Override
    protected void injectComponent(ApplicationComponent applicationComponent) {
        applicationComponent.inject(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent backIntent = new Intent(RestartPopupActivity.this, MainActivity.class);
        backIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(backIntent);
        finish();
    }

    @Override
    public void setAnswerText(String answerText) {
        correctAnswerTextView.setText("Jawapan: " + answerText);
    }

    @Override
    public void setScoreText(int score) {
        popupTextView.setText("Markah anda: " + score);
    }
}
