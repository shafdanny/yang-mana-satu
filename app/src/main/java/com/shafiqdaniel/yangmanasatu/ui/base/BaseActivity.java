package com.shafiqdaniel.yangmanasatu.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.shafiqdaniel.yangmanasatu.BoilerplateApplication;
import com.shafiqdaniel.yangmanasatu.injection.component.ActivityComponent;
import com.shafiqdaniel.yangmanasatu.injection.component.ApplicationComponent;
import com.shafiqdaniel.yangmanasatu.injection.component.ConfigPersistentComponent;
import com.shafiqdaniel.yangmanasatu.injection.component.DaggerConfigPersistentComponent;
import com.shafiqdaniel.yangmanasatu.injection.module.ActivityModule;

/**
 * Created by shafiq on 07/12/2016.
 */

public abstract class BaseActivity extends AppCompatActivity {

    private ActivityComponent mActivityComponent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectComponent(((BoilerplateApplication)getApplication()).getComponent());

        ConfigPersistentComponent configPersistentComponent;
        configPersistentComponent = DaggerConfigPersistentComponent.builder()
                .applicationComponent(BoilerplateApplication.get(this).getComponent())
                .build();
        mActivityComponent = configPersistentComponent.activityComponent(new ActivityModule(this));
    }

    protected abstract void injectComponent(ApplicationComponent applicationComponent);
}
