package com.shafiqdaniel.yangmanasatu.model;

import java.util.Random;

import timber.log.Timber;

/**
 * Created by shafiq on 08/12/2016.
 */


public class GameSession {

    private int mScore;
    private MyCountdownTimer mCountdownTimer;
    private final int timeMax = 5000;
    private int time;
    private CountdownTimerEvent event;

    public GameSession() {
        mScore = 0;
        time = timeMax;
        Timber.i("Game sesion created");
    }

    public void addScore() {
        mScore += 1;
        Timber.i("Score: " + mScore);
    }

    public int getScore() {
        return mScore;
    }

    public void resetSession() {
        mScore = 0;
        if(mCountdownTimer != null)
            mCountdownTimer.cancel();
    }

    public int setCountdownTimer(CountdownTimerEvent timerEvent) {
        if(mCountdownTimer != null) mCountdownTimer.cancel();
        time = timeMax - mScore*50;
        Timber.i("Timer: " + time);
        mCountdownTimer = new MyCountdownTimer(time, 1, timerEvent);
        return time;
    }

    public void startCountdown() {
        mCountdownTimer.start();
    }

    public MyCountdownTimer getCountdownTimer() {
        return mCountdownTimer;
    }

    public void setCountdownTimer(MyCountdownTimer myCountdownTimer) {
        mCountdownTimer = myCountdownTimer;
    }

    public String getCategory() {
        if(mScore > 40) {
            Random rand = new Random();
            int n = rand.nextInt(3);
            if(n==0) return "FIGURES";
            if(n==1) return "LANDMARKS";
            if(n==2) return "FOODS";
        }
        if(mScore > 30) return "FIGURES";
        if(mScore > 20) return "LANDMARKS";
        if(mScore > 10) return "FOODS";
        return "FLAGS";
    }
}
