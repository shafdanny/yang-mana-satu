package com.shafiqdaniel.yangmanasatu.model;

import android.app.Application;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.shafiqdaniel.yangmanasatu.BoilerplateApplication;
import com.shafiqdaniel.yangmanasatu.injection.component.DaggerApplicationComponent;
import com.shafiqdaniel.yangmanasatu.injection.module.ApplicationModule;
import com.shafiqdaniel.yangmanasatu.utils.AnswersLoader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

/**
 *
 * Created by shafiq on 09/12/2016.
 *
 * Contains all the possible answer, grouped according to their category
 *
 */

public class AnswersCollections {

    private List<Answer> answerList;
    private Multimap<String, Answer> answersCollection;

    @Inject
    AnswersLoader answersLoader;

    public AnswersCollections() {

        answersCollection = ArrayListMultimap.create();

        DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule((Application)BoilerplateApplication.getAppContext()))
                .build().inject(this);

        answerList = answersLoader.getAnswerList();
        categorizeAnswer();
    }

    /**
     * Put all answers in a multimap, with their category as key
     */
    public void categorizeAnswer() {
        for(Answer ans: answerList) {
            answersCollection.put(ans.getCategory(), ans);
        }
    }

    public List<Answer> getRandomList(String category, int n) {

        List<Answer> options = (List<Answer>) answersCollection.get(category);

        Collections.shuffle(options);

        List<Answer> answer = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            answer.add(options.get(i));
        }
        return answer;
    }
}
