package com.shafiqdaniel.yangmanasatu.model;

/**
 * Created by shafiq on 08/12/2016.
 */

public interface CountdownTimerEvent {
    void tick(long l);
    void finish();
}
