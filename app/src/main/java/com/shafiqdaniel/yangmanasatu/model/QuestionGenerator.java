package com.shafiqdaniel.yangmanasatu.model;

import android.app.Application;
import android.graphics.drawable.Drawable;

import com.shafiqdaniel.yangmanasatu.BoilerplateApplication;
import com.shafiqdaniel.yangmanasatu.injection.component.DaggerApplicationComponent;
import com.shafiqdaniel.yangmanasatu.injection.module.ApplicationModule;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by shafiq on 08/12/2016.
 */

@Singleton
public class QuestionGenerator {

    @Inject AnswersCollections answersCollections;

    private String correctAnswerString;
    private String previousAnswer;
    private List<String> answerOptions;
    private Drawable flag;

    public QuestionGenerator() {
        DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule((Application) BoilerplateApplication.getAppContext()))
                .build().inject(this);
        correctAnswerString = "";
        previousAnswer = "";
        answerOptions = new ArrayList<>();
    }

    public List<String> getAnswerOptions() {
        return answerOptions;
    }

    public Drawable getFlag() {
        return flag;
    }

    public boolean checkAnswer(String chosenAnswer) {
        return chosenAnswer.equals(correctAnswerString);
    }

    public void generateNextQuestion(String category) {
        previousAnswer = correctAnswerString;
        int nbAnswer = 4;

        List<Answer> answers = answersCollections.getRandomList(category, nbAnswer);
        Random r;
        Answer correctAnswer;
        do {
            r = new Random();
            int rand = r.nextInt(nbAnswer);
            correctAnswer = answers.get(rand);
        } while(correctAnswer.getName().equals(previousAnswer));

        flag = correctAnswer.getDrawable();
        correctAnswerString = correctAnswer.getName();

        answerOptions.clear();
        for(Answer a: answers) {
            answerOptions.add(a.getName());
        }
    }

    public String getCorrectAnswerString() {
        return correctAnswerString;
    }
}
