package com.shafiqdaniel.yangmanasatu.model;

import android.os.CountDownTimer;

/**
 * Created by shafiq on 08/12/2016.
 */

public class MyCountdownTimer extends CountDownTimer {

    CountdownTimerEvent event;

    public MyCountdownTimer(long millisInFuture, long countDownInterval, CountdownTimerEvent event) {
        super(millisInFuture, countDownInterval);
        this.event = event;
    }

    @Override
    public void onTick(long l) {
        event.tick(l);
    }

    @Override
    public void onFinish() {
        event.finish();
    }
}
