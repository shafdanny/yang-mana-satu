package com.shafiqdaniel.yangmanasatu.model;

import android.graphics.drawable.Drawable;

import org.apache.commons.io.FilenameUtils;

/**
 * Created by shafiq on 09/12/2016.
 */

public class Answer {

    private String name;
    private String fileName;
    private String category;
    private Drawable drawable;

    public Answer() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFileName() {
        return fileName;
    }

    /**
     * Deduce the name from the filename by removing the extension
     *
     * @param fileName
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
        String name = FilenameUtils.removeExtension(fileName);
        setName(name);
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Answer: { Name: " + name + ", Filename: " + fileName
                + ", Category: " + category + ", Drawable: " + drawable + " }";
    }
}
