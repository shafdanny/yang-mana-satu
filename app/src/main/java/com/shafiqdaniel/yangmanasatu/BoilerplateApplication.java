package com.shafiqdaniel.yangmanasatu;

import android.app.Application;
import android.content.Context;

import com.shafiqdaniel.yangmanasatu.injection.component.ApplicationComponent;
import com.shafiqdaniel.yangmanasatu.injection.component.DaggerApplicationComponent;
import com.shafiqdaniel.yangmanasatu.injection.module.ApplicationModule;

import timber.log.Timber;

/**
 * Created by shafiq on 06/12/2016.
 */

public class BoilerplateApplication extends Application {

    ApplicationComponent mApplicationComponent;
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
        BoilerplateApplication.context = getApplicationContext();
    }

    public ApplicationComponent getComponent() {

        if(mApplicationComponent == null) {
            mApplicationComponent = DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(this)).build();
        }

        return mApplicationComponent;
    }

    public static BoilerplateApplication get(Context context) {
        return (BoilerplateApplication) context.getApplicationContext();
    }

    public static Context getAppContext() {
        return BoilerplateApplication.context;
    }
}
